package demo.search;

public class BinarySearchDemo {
    public static void main(String[] args) {
        int[] arr = {23, 44, 56, 65, 76, 77, 89, 102};

        int target = 89;
        int result = binarySearch(arr, target);
        System.out.println("Result is " + result);
    }


    static int binarySearch(int[] arr, int target) {

        int mid, low, high;
        low = 0;
        high = arr.length - 1;

        while (low <= high) {
            //mid=(low+high)/2;
            mid = low + ((high - low) / 2);
            if (arr[mid] == target) {
                return mid;
            } else if (target > arr[mid]) {
                low = mid + 1;
            } else if (target < arr[mid]) {
                high = mid - 1;
            }

        }

        return -1;
    }

}
